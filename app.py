import seetree.gsutil as gsutil
import json 
from google.cloud import storage
import csv
import os, sys

dict_count={}

src_path = "gs://seetree-sandbox-dev/sre-onboard/ex3/"
dst_path = "/home/user/Desktop/google-cloud-sdk-326.0.0-linux-x86_64"
gsutil.cp(os.path.join(src_path,"*.geojson"), dst_path)


#Iterating through the files
# Open a file
path = dst_path 
dirs = os.listdir( path )
counter=0
# This would print all the files and directories
for file in dirs:
   name, extension = os.path.splitext(file)
   if extension==".geojson":
       counter=0
       with open(file, 'r') as f:
        parsed_json = json.load(f)
        for line in parsed_json['features'] :
            if line['properties']['detected'] == 'Yes':
                        counter+=1 
    
        dict_count[name]=counter
    
with open('cipores.csv', mode='w') as csv_file:
        fieldnames = ['grove name', 'trees with Cipo']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for element in dict_count:
                writer.writerow({'grove name': element, 'trees with Cipo': dict_count[element]})

        
gsutil.cp("/home/user/Desktop/google-cloud-sdk-326.0.0-linux-x86_64/cipores.csv","gs://seetree-sandbox-dev/Aishy/ex3")